package resource;

import lombok.RequiredArgsConstructor;

import javax.swing.*;

/**
 * Herzstück für alle Pflanzen die im Programm abgebildet werden.
 * Da Pflanzen sich lediglich in ihren konstanten grafischen Repräsentationen und ihren konstanten Wachstumsgrenzen
 * unterscheiden, wurde ein enum gewählt.
 */

@RequiredArgsConstructor
public enum PlantResource
{

   WHEAT(12.07, 23.22, PlantImageIconResource.WHEAT),
   CORN(8.7, 13.37, PlantImageIconResource.CORN);

   private final double minimalGrowthInCmPerCycle;
   private final double maximumGrowthInCmPerCycle;
   private final PlantImageIconResource plantImageResource;

   public double getMinimalGrowthInCmPerCycle()
   {
      return minimalGrowthInCmPerCycle;
   }

   public double getMaximumGrowthInCmPerCycle()
   {
      return maximumGrowthInCmPerCycle;
   }

   public ImageIcon getFirstStadiumImageIcon()
   {
      return plantImageResource.getFirstStadiumIcon();
   }

   public ImageIcon getSecondStadiumImageIcon()
   {
      return plantImageResource.getSecondStadiumIcon();
   }

   public ImageIcon getThirdStadiumImageIcon()
   {
      return plantImageResource.getFinalStadiumIcon();
   }

}
