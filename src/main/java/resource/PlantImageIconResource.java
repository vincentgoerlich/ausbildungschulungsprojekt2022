package resource;

import lombok.Getter;

import javax.swing.*;

/**
 * Zentrale Sammelstelle von {@link ImageIcon} für Pflanzen.
 * Nutzt die in {@link PlantImageIconPathResource} hinterlegten Bilddateipfade und baut daraus {@link ImageIcon}s
 */
@Getter
public enum PlantImageIconResource
{
   WHEAT(PlantImageIconPathResource.WHEAT_STADIUM_1, PlantImageIconPathResource.WHEAT_STADIUM_2, PlantImageIconPathResource.WHEAT_STADIUM_3),
   CORN(PlantImageIconPathResource.CORN_STADIUM_1,PlantImageIconPathResource.CORN_STADIUM_2,PlantImageIconPathResource.CORN_STADIUM_3);


   public static final ImageIcon BASE_STADIUM_ICON = new ImageIcon(PlantImageIconPathResource.GENERIC_STADIUM_0.getPath());
   private final ImageIcon firstStadiumIcon;
   private final ImageIcon secondStadiumIcon;
   private final ImageIcon finalStadiumIcon;

   PlantImageIconResource(PlantImageIconPathResource firstStadiumIcon, PlantImageIconPathResource secondStadiumIcon, PlantImageIconPathResource finalStadiumIcon)
   {
      this.firstStadiumIcon = new ImageIcon(firstStadiumIcon.getPath());
      this.secondStadiumIcon = new ImageIcon(secondStadiumIcon.getPath());
      this.finalStadiumIcon = new ImageIcon(finalStadiumIcon.getPath());
   }
}
