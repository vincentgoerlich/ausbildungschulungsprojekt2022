package resource;

import gui.util.ImageResizer;

import javax.swing.*;

/**
 * Zentrale Verwaltung von MenuImageIcon Pfaden
 */
public enum MenuImageIconResource {

    FULL_CYCLE_JOB("src/main/java/resource/graphics/jobs.jpg"),
    EXPORT_JSON("src/main/java/resource/graphics/exportjson.png"),
    RACOON("src/main/java/resource/graphics/racoon.png");

    private final ImageIcon menuIcon;

    MenuImageIconResource(String path) {

        this.menuIcon = ImageResizer.to(new ImageIcon(path),100,100);

    }

    public ImageIcon getIcon() {
        return menuIcon;
    }

}
