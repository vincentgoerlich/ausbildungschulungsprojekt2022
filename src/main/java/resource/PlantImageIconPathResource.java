package resource;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Zentrale Sammelstelle von allen Pflanzen-Bilddateipfaden
 */
@RequiredArgsConstructor
@Getter
public enum PlantImageIconPathResource
{
   GENERIC_STADIUM_0("src/main/java/resource/graphics/Earth.png"),
   CORN_STADIUM_1("src/main/java/resource/graphics/CornStadium1.png"),
   CORN_STADIUM_2("src/main/java/resource/graphics/CornStadium2.png"),
   CORN_STADIUM_3("src/main/java/resource/graphics/CornStadium3.png"),
   WHEAT_STADIUM_1("src/main/java/resource/graphics/WheatStadium1.png"),
   WHEAT_STADIUM_2("src/main/java/resource/graphics/WheatStadium2.png"),
   WHEAT_STADIUM_3("src/main/java/resource/graphics/WheatStadium3.png");

   private final String path;

}
