package dao;

import entity.AbstractPlantEntity;
import entity.PlantEntityPrototypes;
import resource.PlantResource;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * Klasse zum Speichern & Laden von {@link AbstractPlantEntity}-Instanzen nach .csv
 */
public class CsvDao implements Dao
{
   private static final DaoLogger CSV_DAO_LOG = DaoLogger.of(CsvDao.class);
   /**
    * Im gesplitteten String[] beim Laden hat die {@link PlantResource} index = 0
    */
   private static final int PLANT_RESOURCE_INDEX = 0;
   /**
    * Im gesplitteten String[] beim Laden hat die Höhe einer Pflanze index = 1
    */
   private static final int PLANT_HEIGHT_INDEX = 1;
   /**
    * Default Speicher- & Ladedateipfad
    */
   private static final String DEFAULT_PATH = "src/main/java/files/";

   private static final String DELIMITER = ";";
   /**
    * Map, die jeder {@link PlantResource}-Konstante eine {@link File} zuordnet und korrespondierend benennt.
    */
   private static EnumMap<PlantResource, File> FILE_MAP;

   /**
    * Generisches Speichern, dass je nach {@link AbstractPlantEntity}-Instanz automatisch die richtige .csv-Datei mit den Werten des Inputs überschreibt
    * <b>DAHER KEIN exits()-check notwendig</b>, erzeugt automatisch neue Datei.
    *
    * @param plantEntities  entities die gespeichert werden sollen
    * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanz
    */
   @Override
   public <PLANT_ENTITY extends AbstractPlantEntity> void save(List<PLANT_ENTITY> plantEntities)
   {
      lazyLoadFileMapEntries();
      persistEntities(plantEntities);
   }

   // Protected, damit gemockt werden kann.
   protected <PLANT_ENTITY extends AbstractPlantEntity> void persistEntities(List<PLANT_ENTITY> plantEntities)
   {
      File outputFile = autoChooseFile(plantEntities.get(0).getPlantResource());
      CSV_DAO_LOG.saveStart(outputFile.getAbsolutePath());
      try(PrintWriter printWriter = new PrintWriter(outputFile))
      {
         writeEntities(printWriter, plantEntities);
         printWriter.flush();
         CSV_DAO_LOG.saveSuccessful(outputFile.getAbsolutePath());
      }
      catch(IOException e)
      {
         CSV_DAO_LOG.saveFailed(outputFile.getAbsolutePath(), e);
         e.printStackTrace();
      }
      CSV_DAO_LOG.saveFinish(outputFile.getAbsolutePath());
   }

   /**
    * Generisches Laden, dass je nach {@link PlantResource}-Input automatisch die korrespondierende Datei ausließt zu einer Liste von
    * {@link AbstractPlantEntity}-Instanzen konvertiert.
    *
    * @param plantResource  pflanzenResource die geladen werden soll.
    * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanz
    * @return Liste von PlantEntities aus der jeweiligen csv Datei
    */
   @Override
   public <PLANT_ENTITY extends AbstractPlantEntity> List<PLANT_ENTITY> load(PlantResource plantResource)
   {
      createFileDirectoryIfDoNotExit();
      lazyLoadFileMapEntries();
      File sourceFile = autoChooseFile(plantResource);
      return tryToLoadAndConvertOrEmptyList(sourceFile);
   }

   // Protected, damit gemockt werden kann.
   protected <PLANT_ENTITY extends AbstractPlantEntity> List<PLANT_ENTITY> tryToLoadAndConvertOrEmptyList(File sourceFile)
   {
      createFileDirectoryIfDoNotExit();
      CSV_DAO_LOG.loadStart(sourceFile.getAbsolutePath());
      List<PLANT_ENTITY> loadedPlantEntities = new ArrayList<>();

      try(BufferedReader br = new BufferedReader(new FileReader(sourceFile)))
      {
         String line;
         while((line = br.readLine()) != null)
         {
            String[] values = line.split(";");
            PLANT_ENTITY plantEntity = convertToPlantEntity(values[PLANT_RESOURCE_INDEX], values[PLANT_HEIGHT_INDEX]);
            loadedPlantEntities.add(plantEntity);
         }
         CSV_DAO_LOG.loadSuccessful(sourceFile.getAbsolutePath(), loadedPlantEntities);
      }
      catch(IOException ex)
      {
         CSV_DAO_LOG.loadFailed(sourceFile.getAbsolutePath(), ex);
      }
      CSV_DAO_LOG.loadFinish(sourceFile.getAbsolutePath());
      return loadedPlantEntities;
   }

   private <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY convertToPlantEntity(String plantResource, String currenHeight)
   {
      PlantResource loadedPlantResource = PlantResource.valueOf(plantResource);
      double loadedHeight = Double.parseDouble(currenHeight);
      PLANT_ENTITY plantEntity = PlantEntityPrototypes.of(loadedPlantResource);
      plantEntity.setCurrentHeight(loadedHeight);
      return plantEntity;
   }

   private <PLANT_ENTITY extends AbstractPlantEntity> void writeEntities(PrintWriter printWriter, List<PLANT_ENTITY> toBeSaved)
   {
      for(PLANT_ENTITY plantEntity : toBeSaved)
      {
         String plantResourceAsString = plantEntity.getPlantResource().name();
         printWriter.print(plantResourceAsString);
         printWriter.print(DELIMITER);
         printWriter.print(plantEntity.getCurrentHeight());
         printWriter.println();
      }
   }

   private static void lazyLoadFileMapEntries()
   {
      if(FILE_MAP == null)
      {
         FILE_MAP = new EnumMap<>(PlantResource.class);
         List<PlantResource> plantResources = List.of(PlantResource.values());
         for(PlantResource plantResource : plantResources)
         {
            FILE_MAP.put(plantResource, new File(formatFileName(plantResource)));
         }
      }
   }

   private void createFileDirectoryIfDoNotExit()
   {
      try
      {
         Files.createDirectory(Path.of(DEFAULT_PATH));
         CSV_DAO_LOG.createDirectory(DEFAULT_PATH);
      }
      catch(IOException e)
      {
         CSV_DAO_LOG.directoryExists(DEFAULT_PATH);
      }
   }

   // Protected, damit gemockt werden kann.
   protected File autoChooseFile(PlantResource plantResource)
   {
      return FILE_MAP.get(plantResource);
   }

   private static String formatFileName(PlantResource plantResource)
   {
      return DEFAULT_PATH + plantResource.name() + ".csv";
   }
}
