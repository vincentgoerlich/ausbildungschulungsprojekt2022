package dao;

import entity.AbstractPlantEntity;
import resource.PlantResource;

import java.util.List;

/**
 * Interface für alle Speicher und LadeKlassen. (YAGNI-Vorwurf ist berechtigt, programmiere dennoch aufn Interface hin für Erweiterbarkeit)
 */
public interface Dao
{
   <PLANT_ENTITY extends AbstractPlantEntity> void save(List<PLANT_ENTITY> plantEntities);
   <PLANT_ENTITY extends AbstractPlantEntity> List<PLANT_ENTITY> load(PlantResource plantResource);
}
