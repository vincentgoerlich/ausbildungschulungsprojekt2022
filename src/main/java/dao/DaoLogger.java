package dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Logger für alle DAO-Implementierungen
 */
public class DaoLogger
{
   private static Logger logger = LogManager.getLogger();

   public static DaoLogger of(Class<? extends Dao> clazz)
   {
      return new DaoLogger(clazz);
   }

   private DaoLogger(Class<? extends Dao> clazz)
   {
      logger = LogManager.getLogger(clazz);
   }

   public void saveSuccessful(String path)
   {
      String msg = "Successfully saved to " + path;
      logger.info(msg);
   }

   public void loadFailed(String path, Exception exception)
   {
      String msg = "Loading from " + path + " failed. " + exception.getClass();
      logger.error(msg);
   }

   public void saveFailed(String path, Exception exception)
   {
      String msg = "Saving to " + path + " failed. " + exception.getClass();
      logger.error(msg);
   }

   public void loadSuccessful(String path, List<?> loadedList)
   {
      String msg = "Loading from " + path + " successful. Size: " + loadedList.size();
      logger.info(msg);
   }

   public void saveStart(String path)
   {
      String msg = "Starting saving process to " + path;
      logger.info(msg);
   }

   public void loadStart(String path)
   {
      String msg = "Starting loading process from " + path;
      logger.info(msg);
   }

   public void saveFinish(String path)
   {
      String msg = "Saving process to " + path + " finished";
      logger.info(msg);
   }

   public void loadFinish(String path)
   {
      String msg = "Loading process from " + path + " finished";
      logger.info(msg);
   }

   public void createDirectory(String path)
   {
      String msg = "New Directory " + path + " created";
      logger.info(msg);
   }

   public void directoryExists(String path)
   {
      String msg = "Directory " + path + " already exists";
      logger.info(msg);
   }
}
