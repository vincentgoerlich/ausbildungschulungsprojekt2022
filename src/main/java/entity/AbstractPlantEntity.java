package entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import resource.PlantResource;

/**
 * Parent Klasse für alle Pflanzen Entities.
 */
@Getter
@Setter
@Accessors(chain = true)
public abstract class AbstractPlantEntity{

   private PlantResource plantResource;
   private double currentHeight;

   /**
    * Höhe eines neuen Pflanzenobjekts ist 0.0 by default.
    */
   protected AbstractPlantEntity(){
      currentHeight = 0.0;
   }

   protected AbstractPlantEntity(PlantResource plantResource){
      this.plantResource = plantResource;
   }

   /**
    * Alle Instanzen dieser Klasse müssen die Fähigkeit implementieren, von sich selbst eine typsichere Kopie zu erstellen.
    * @param <PLANT_ENTITY> Instanz von {@link AbstractPlantEntity}
    * @return ein neues, mit identischen Werten kopiertes, typsicheres Pflanzenobjekt.
    * @apiNote Essentiell für {@link PlantEntityPrototypes}
    */
   public abstract <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY copy();




}
