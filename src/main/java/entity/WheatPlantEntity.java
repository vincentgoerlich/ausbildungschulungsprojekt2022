package entity;

import resource.PlantResource;

/**
 * Entity für Weizenpflanzen
 * @apiNote Instanziierung via {@link PlantEntityPrototypes}
 */
public final class WheatPlantEntity extends AbstractPlantEntity
{
   WheatPlantEntity()
   {
      super(PlantResource.WHEAT);
   }

   @SuppressWarnings("unchecked")
   @Override
   public <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY copy()
   {
      return (PLANT_ENTITY) new WheatPlantEntity().setPlantResource(this.getPlantResource()).setCurrentHeight(this.getCurrentHeight());
   }


}
