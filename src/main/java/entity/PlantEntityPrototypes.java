package entity;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import resource.PlantResource;

import java.util.Map;

/**
 * Klasse mit der Verantwortlichkeit neue {@link AbstractPlantEntity}-Instanzen erzeugen zu können.
 * Macht dabei Gebrauch von der {@link AbstractPlantEntity#copy()}-Implementierungen der Instanzen.
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PlantEntityPrototypes
{
   /**
    * Map mit Prototypen der bisher existierenden {@link AbstractPlantEntity}-Instanzen
    */
   private static final Map<PlantResource, AbstractPlantEntity> PLANT_ENTITY_PROTOTYPE_MAP =
      Map.of(
         PlantResource.CORN, new CornPlantEntity(),
         PlantResource.WHEAT, new WheatPlantEntity()
            );

   /**
    * Klont/kopiert die zum Parameter {@link PlantResource} gemappte {@link AbstractPlantEntity}-Instanz der PLANT_ENTITY_PROTOTYPE_MAP
    * und returned sie.
    * @param plantResource key für den korrespondierenden Prototyp
    * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanz
    * @return Wenn {@link PlantResource} der Map bekannt, liefert die passende {@link AbstractPlantEntity}-Instanz zurück. Falls enum Konstante noch nicht
    * gemappt, throwt {@link IllegalArgumentException}
    */
   public static <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY of(PlantResource plantResource)
   {
      if(PLANT_ENTITY_PROTOTYPE_MAP.containsKey(plantResource)){

         return PLANT_ENTITY_PROTOTYPE_MAP.get(plantResource).copy();
      }
      throw new IllegalArgumentException(plantResource+" not mapped yet.");
   }
}
