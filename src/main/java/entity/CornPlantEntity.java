package entity;

import resource.PlantResource;

/**
 * Entity für Maispflanzen
 * @apiNote Instanziierung via {@link PlantEntityPrototypes}
 */
public final class CornPlantEntity extends AbstractPlantEntity
{
   /**
    * Haben bei Instanzierung bereits {@link PlantResource}-member auf CORN gesetzt
    */
   CornPlantEntity()
   {
      super(PlantResource.CORN);
   }

   @SuppressWarnings("unchecked")
   @Override
   public <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY copy()
   {
      return (PLANT_ENTITY) new CornPlantEntity().setPlantResource(this.getPlantResource())
         .setCurrentHeight(this.getCurrentHeight());
   }

}
