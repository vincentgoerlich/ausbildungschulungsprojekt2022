package gui.plant;

import entity.AbstractPlantEntity;
import gui.util.ImageResizer;
import resource.PlantImageIconResource;
import resource.PlantResource;

import javax.swing.*;

/**
 * Controller für {@link PlantModel}, verantwortlich für Koordination von Wachstum und Höhen-spezifischer, grafischer Darstellung
 *
 * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanzen
 */
public class PlantController<PLANT_ENTITY extends AbstractPlantEntity>
{
   private PlantModel<PLANT_ENTITY> plantModel;
   private final PlantView plantView;

   public PlantController()
   {
      this.plantView = new PlantView();
      this.plantModel = null;
      pushToView();
   }

   public PlantController(PlantModel<PLANT_ENTITY> plantModel)
   {
      this();
      setPlantModel(plantModel);
   }

   /**
    * Lässt eine Pflanze wachsen um bestimmte Höhe, sofern das {@link PlantController#plantModel} nicht null ist, meldet die Änderung an die View
    * @param bonusHeight Höhe, die zugenommen werden soll.
    */
   public void letPlantGrow(double bonusHeight)
   {
      if(this.plantModel != null)
      {
         this.plantModel.addToHeight(bonusHeight);
      }
      pushToView();
   }

   public void setPlantModel(PlantModel<PLANT_ENTITY> plantModel)
   {
      this.plantModel = plantModel;
      pushToView();
   }

   private void pushToView()
   {
      updateImageIcon();
      updateToolTip();
   }


   private void updateImageIcon(){
      ImageIcon updatedImageIcon = chooseImageIconAccordingToHeight();
      this.plantView.changeIcon(ImageResizer.to(updatedImageIcon,70, 70));
   }

   private void updateToolTip(){
      String toolTipText  = "Height: "+ (plantModel == null ? "Only Earth" : plantModel.getCurrentHeight()+"cm");
      this.plantView.addToolTip(toolTipText);
   }

   public JLabel getView()
   {
      return this.plantView.getView();
   }

   /**
    * Auto selektiert das zur Höhe passende ImageIcon aus der {@link PlantResource}s {@link PlantImageIconResource}
    */
   private ImageIcon chooseImageIconAccordingToHeight()
   {
      if(plantModel == null || plantModel.getCurrentHeight() == 0.0)
      {
         return PlantImageIconResource.BASE_STADIUM_ICON;
      }
      else if(plantModel.getCurrentHeight() < 30.00)
      {
         return plantModel.getPlantResource().getFirstStadiumImageIcon();
      }
      else if(plantModel.getCurrentHeight() < 60)
      {
         return plantModel.getPlantResource().getSecondStadiumImageIcon();
      }
      return plantModel.getPlantResource().getThirdStadiumImageIcon();
   }

   public boolean plantModelIsNull()
   {
      return plantModel == null;
   }

   public PlantModel<PLANT_ENTITY> getPlantModel()
   {
      return plantModel;
   }


   private static class PlantView
   {
      private JLabel jLabel;

      public PlantView()
      {
         initView();
      }

      private void initView()
      {
         jLabel = new JLabel();
      }

      public void changeIcon(ImageIcon imageIcon)
      {
         jLabel.setIcon(imageIcon);
      }

      public JLabel getView()
      {
         return jLabel;
      }

      public void addToolTip(String toolTipText)
      {
         jLabel.setToolTipText(toolTipText);
      }

   }
}
