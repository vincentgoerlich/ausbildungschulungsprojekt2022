package gui.plant;

import entity.AbstractPlantEntity;
import entity.CornPlantEntity;
import entity.PlantEntityPrototypes;
import entity.WheatPlantEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import resource.PlantResource;

import java.util.Map;

/**
 * Klasse mit der Verantwortlichkeit neue {@link PlantModel}s erzeugen zu können.
 * Macht dabei Gebrauch von der {@link PlantModel#copy()}-Implementierungen der Instanzen.
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PlantModelPrototypes
{

   private static final Map<PlantResource, PlantModel<? extends AbstractPlantEntity>> PLANT_MODEL_PROTOTYPE_MAP =
      Map.of(
         PlantResource.CORN, new PlantModel<CornPlantEntity>().setPlantEntity(PlantEntityPrototypes.of(PlantResource.CORN)),
         PlantResource.WHEAT, new PlantModel<WheatPlantEntity>().setPlantEntity(PlantEntityPrototypes.of(PlantResource.WHEAT))
            );

   /**
    * Erstellt und returned einen {@link PlantModel}-Klon mit passender default {@link AbstractPlantEntity}-Instanz
    * @param plantResource key für das korrespondierende {@link PlantModel}
    * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanz
    * @return  {@link PlantModel}-Klon mit passender default {@link AbstractPlantEntity}-Instanz
    */
   @SuppressWarnings("unchecked")
   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantModel<PLANT_ENTITY> of(PlantResource plantResource)
   {
      if(!PLANT_MODEL_PROTOTYPE_MAP.containsKey(plantResource)){
         throw new IllegalArgumentException(plantResource+" not implemented yet");
      }
      return  (PlantModel<PLANT_ENTITY>) PLANT_MODEL_PROTOTYPE_MAP.get(plantResource).copy();
   }

   /**
    * Erstellt und returned einen {@link PlantModel}-Klon mit input {@link AbstractPlantEntity}-Instanz
    * @param plantEntity {@link AbstractPlantEntity}-Instanz für das Model
    * @param <PLANT_ENTITY> {@link AbstractPlantEntity}-Instanz
    * @return  {@link PlantModel}-Klon mit input {@link AbstractPlantEntity}-Instanz
    */
   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantModel<PLANT_ENTITY> withEntity(PLANT_ENTITY plantEntity){
      PlantModel<PLANT_ENTITY> cloneWithSpecificEntity = of(plantEntity.getPlantResource());
      cloneWithSpecificEntity.setPlantEntity(plantEntity);
      return cloneWithSpecificEntity;
   }

}
