package gui.plant;

import entity.AbstractPlantEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resource.PlantResource;

/**
 * Generisches Model für alle {@link AbstractPlantEntity}-Instanzen.
 * Regelt die Wachstumszunahme.
 */
@Getter
@Setter
@Accessors(chain = true)
public class PlantModel<PLANT_ENTITY extends AbstractPlantEntity>
{
   private static final Logger LOG = LogManager.getLogger(PlantModel.class);
   private PLANT_ENTITY plantEntity;

   public PlantModel()
   {
   }

   public PlantModel(PLANT_ENTITY plantEntity)
   {
      this.plantEntity = plantEntity;
   }

   public void addToHeight(double bonusHeight)
   {
      double beforeGrowing = getCurrentHeight();
      plantEntity.setCurrentHeight(plantEntity.getCurrentHeight() + bonusHeight);
      String msg = plantEntity.getPlantResource().name() + " grew from "+beforeGrowing+"cm to "+getCurrentHeight()+"cm";
      LOG.info(msg);
   }

   public double getCurrentHeight()
   {
      return this.plantEntity.getCurrentHeight();
   }

   public PlantResource getPlantResource()
   {
      return this.plantEntity.getPlantResource();
   }

   /**
    * @return eine exakte Kopie des aktuellen Models als neues Object samt ebenfalls kopierten Entity-Objekt
    */
   public PlantModel<PLANT_ENTITY> copy()
   {
      return new PlantModel<>(this.plantEntity.copy());
   }
}
