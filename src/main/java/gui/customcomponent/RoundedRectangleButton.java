package gui.customcomponent;

import gui.util.ImageResizer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.RoundRectangle2D;

/**
 * Erweiterung eines {@link JButton}s, der mit abgerundeten Ecken dargestellt wird.
 */
public class RoundedRectangleButton extends JButton
{
   private Shape shape;

   public RoundedRectangleButton(){
      initDefaults();
   }

   private void initDefaults()
   {
      setBackground(Color.WHITE);
      setVerticalTextPosition(BOTTOM);
      setHorizontalTextPosition(CENTER);
      setFocusPainted(false);
      setContentAreaFilled(false);
      // Mouse-Over Icon Animation
      addMouseListener(new MouseAdapter() {
         @Override
         public void mouseEntered(MouseEvent e) {
            if(getIcon() != null){
               int xOfIcon = getIcon().getIconHeight();
               setIcon(ImageResizer.to((ImageIcon) getIcon(),xOfIcon+20,xOfIcon+20));
            }
         }

         @Override
         public void mouseExited(MouseEvent e) {
            if(getIcon() != null){
               int xOfIcon = getIcon().getIconHeight();
               setIcon(ImageResizer.to((ImageIcon) getIcon(),xOfIcon-20,xOfIcon-20));
            }
         }
      });
   }

   @Override
   protected void paintComponent(Graphics g) {

      if (getModel().isArmed()) {
         g.setColor(Color.lightGray);
      } else {
         g.setColor(getBackground());
      }
      g.fillRoundRect(0, 0, getWidth()-1, getHeight()-1, 15, 15);
      super.paintComponent(g);

   }
   protected void paintBorder(Graphics g) {
      g.setColor(getForeground());
      g.drawRoundRect(0, 0, getWidth()-3, getHeight()-3, 15, 15);
   }

   public boolean contains(int x, int y) {
      if (shape == null || !shape.getBounds().equals(getBounds())) {
         shape = new RoundRectangle2D.Float(0, 0, getWidth() - 1, getHeight() - 1, 15, 15);
      }
      return shape.contains(x, y);
   }

}
