package gui.frame;

import gui.ApplicationController;
import gui.util.ConstraintsUtil;
import resource.MenuImageIconResource;

import javax.swing.*;
import java.awt.*;

/**
 * Darstellung-Frame für das Programm als {@link JFrame}-Erweiterung
 */
public class ApplicationFrame extends JFrame
{
   private static final String PROGRAM_NAME = "Farm Simulator";
   private static final JPanel CONTENT_PANE = new JPanel(new GridBagLayout());

   /**
    * Stellt den fertigen Frame samt {@link ApplicationController} bereit.
    */
   public ApplicationFrame()
   {
      super(PROGRAM_NAME);
      setLayout(new GridBagLayout());
      setIconImage(MenuImageIconResource.FULL_CYCLE_JOB.getIcon().getImage());
      CONTENT_PANE.setBackground(Color.WHITE);
      setExtendedState(MAXIMIZED_BOTH);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      setContentPane(CONTENT_PANE);
      getContentPane().add(new ApplicationController().getView(), ConstraintsUtil.constraints(0, 0));
      setVisible(true);
   }


}
