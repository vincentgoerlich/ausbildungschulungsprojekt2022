package gui.job;

import gui.farm.FarmStatsPanel;
import gui.plantfield.PlantFieldController;

import java.util.List;
import java.util.function.Supplier;

/**
 * Wrapper-Klasse ohne neue Funktionalität, führt lediglich alle Programm-Zyklus relevanten Jobs in folgender Reihenfolge aus:
 * <ol>
 *    <li> {@link SowJob}</li>
 *    <li> {@link WaterJob}</li>
 *    <li> {@link HarvestJob}</li>
 *    <li> {@link CsvSaveJob}</li>
 * </ol>
 */
public class FullCycleJob implements Job
{
   private static Supplier<FarmStatsPanel> STATS_UPDATE;
   private static final JobLogger JOB_LOGGER = JobLogger.of(FullCycleJob.class);
   /**
    * Statische Liste von Jobs der für einen kompletten Programm-Zyklus.
    */
   private static final List<Job> ALL_JOBS = List.of(new SowJob(), new WaterJob(), new HarvestJob(), new CsvSaveJob());

   public FullCycleJob(Supplier<FarmStatsPanel> statsPanelControllerConsumer)
   {
      STATS_UPDATE = statsPanelControllerConsumer;
   }

   @Override
   public void execute(PlantFieldController<?> plantfieldCtrl)
   {
      JOB_LOGGER.jobStarted();
      for(Job job : ALL_JOBS)
      {
         job.execute(plantfieldCtrl);
      }
      JOB_LOGGER.jobFinished();
      STATS_UPDATE.get().update();
   }
}
