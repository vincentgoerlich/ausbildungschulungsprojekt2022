package gui.job;

import gui.plant.PlantController;
import gui.plant.PlantModelPrototypes;
import gui.plantfield.PlantFieldController;

/**
 * Ist verantwortlich dafür null-{@link gui.plant.PlantModel}s innerhalb eines {@link PlantFieldController} mit einem vorgefertigten, neuen Model zu befüllen.
 * @apiNote null Plantmodels gelten als "geerntet" & somit als neu "beflanzbar"
 */
public class SowJob implements Job
{
   private static final JobLogger JOB_LOGGER = JobLogger.of(SowJob.class);

   @Override
   public void execute(PlantFieldController<?> plantfieldCtrl)
   {
      JOB_LOGGER.jobStarted();
      int timesExecuted = 0;
      for(PlantController<?> plantController : plantfieldCtrl.getPlantControllers())
      {
         if(plantController.plantModelIsNull()){
            plantController.setPlantModel(PlantModelPrototypes.of(plantfieldCtrl.getPlantResource()));
            timesExecuted++;
         }
      }
      JOB_LOGGER.jobExecutedFor(plantfieldCtrl.getPlantResource(),timesExecuted);
      JOB_LOGGER.jobFinished();
   }

}
