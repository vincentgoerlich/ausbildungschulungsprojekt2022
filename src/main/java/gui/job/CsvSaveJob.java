package gui.job;

import dao.CsvDao;
import entity.AbstractPlantEntity;
import gui.plant.PlantController;
import gui.plant.PlantModel;
import gui.plantfield.PlantFieldController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Mapped ein {@link PlantFieldController}-Objekt auf vorhandene {@link AbstractPlantEntity}-Instanzen und leitet
 * diese als Liste an {@link CsvDao} weiter.
 */
public class CsvSaveJob implements Job
{
   private static final CsvDao CSV_DAO = new CsvDao();

   @Override
   public void execute(PlantFieldController<?> plantfieldCtrl)
   {
      List<? extends AbstractPlantEntity> list = plantfieldCtrl.getPlantControllers().stream()
         .map(PlantController::getPlantModel)
         .filter(Objects::nonNull)
         .map(PlantModel::getPlantEntity)
         .collect(Collectors.toList());
      CSV_DAO.save(list);
   }
}
