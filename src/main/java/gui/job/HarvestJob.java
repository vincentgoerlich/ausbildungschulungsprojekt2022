package gui.job;

import gui.plant.PlantController;
import gui.plantfield.PlantFieldController;
import resource.PlantResource;

/**
 * Filtert alle {@link PlantController} nach {@link gui.plant.PlantModel}s, deren {@link entity.AbstractPlantEntity}-Instanz
 * eine Höhe von >= 100.00 hat und setzt das jeweilige {@link gui.plant.PlantModel} auf null.
 */
public class HarvestJob implements Job
{
   private static final JobLogger JOB_LOG = JobLogger.of(HarvestJob.class);
   private static int CORN_HARVESTED = 0;
   private static int WHEAT_HARVESTED = 0;


   @Override
   public void execute(PlantFieldController<?> plantfieldCtrl)
   {
      JOB_LOG.jobStarted();
      int timesExecuted = 0;
      for(PlantController<?> plantController : plantfieldCtrl.getPlantControllers())
      {
         if(isReadyToBeHarvested(plantController))
         {
            timesExecuted++;
            plantController.setPlantModel(null);
            upCountHarvestCounter(plantfieldCtrl.getPlantResource());
         }

      }
      JOB_LOG.jobExecutedFor(plantfieldCtrl.getPlantResource(),timesExecuted);
      JOB_LOG.jobFinished();
   }

   private boolean isReadyToBeHarvested(PlantController<?> plantController)
   {
      if(plantController.getPlantModel() != null){
         return plantController.getPlantModel().getCurrentHeight() >= 100;
      }
      return false;
   }

   private void upCountHarvestCounter(PlantResource plantResource)
   {
      switch(plantResource)
      {
         case CORN:
            CORN_HARVESTED++;
            break;
         case WHEAT:
            WHEAT_HARVESTED++;
            break;
      }
   }

   public static int getCornHarvested()
   {
      return CORN_HARVESTED;
   }

   public static int getWheatHarvested()
   {
      return WHEAT_HARVESTED;
   }
}
