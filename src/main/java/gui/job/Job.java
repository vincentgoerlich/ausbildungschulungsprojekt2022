package gui.job;

import gui.plantfield.PlantFieldController;

/**
 * Interface für alle Job-Implementierungen. Hätte man auch als {@link java.util.function.Consumer}, sprich, als lambdas lösen können.
 */
public interface Job
{
   void execute(PlantFieldController<?> plantfieldCtrl);
}
