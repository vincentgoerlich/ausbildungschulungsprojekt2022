package gui.job;

import gui.plant.PlantController;
import gui.plantfield.PlantFieldController;
import resource.PlantResource;

/**
 * Berechnet das randomisierte Wachstum in cm von {@link entity.AbstractPlantEntity}-Instanzen und triggert
 * bei dem zugehörigen {@link PlantController#letPlantGrow(double)}
 */
public class WaterJob implements Job
{
   private static final JobLogger JOB_LOGGER = JobLogger.of(WaterJob.class);

   /**
    * Berechnet das randomisierte Wachstum in cm von {@link entity.AbstractPlantEntity}-Instanzen und triggert
    * bei dem zugehörigen {@link PlantController#letPlantGrow(double)}
    * @param plantfieldCtrl der alle {@link PlantController} eines bestimmten Typs hält.
    */
   @Override
   public void execute(PlantFieldController<?> plantfieldCtrl)
   {
      JOB_LOGGER.jobStarted();
      for(PlantController<?> plantController : plantfieldCtrl.getPlantControllers())
      {
         double bonusHeight = getRandomGrowthInCM(plantfieldCtrl.getPlantResource());
         plantController.letPlantGrow(bonusHeight);
      }
      JOB_LOGGER.jobFinished();
   }


   /**
    * Berechnet anhand der gesetzten Wachstumsgrenzen der {@link PlantResource}
    * ein zufälliges, auf 2 Nachkommastellen gerundetes Wachstum in cm.
    * @param plantResource {@link PlantResource}, aus welcher der minimale & maximale Wachstumswert entnommen wird.
    * @return Wachstum in cm (double).
    */
   private static double getRandomGrowthInCM(PlantResource plantResource)
   {
      double minimumGrowthOfConcretePlant = plantResource.getMinimalGrowthInCmPerCycle();
      double maximumGrowthOfConcretePlant = plantResource.getMaximumGrowthInCmPerCycle();
      return generateRoundedRandomInBounds(minimumGrowthOfConcretePlant, maximumGrowthOfConcretePlant);
   }

   /**
    * Generiert eine Zufallszahl, gerundet auf zwei Nachkommastellen, zwischen den beiden input Werten.
    * @param lowerBound der Minimalwert, den die Zufallszahl annehmen darf
    * @param upperBound der Maximalwert, den die Zufallszahl annehmen darf.
    * @return Zufallszahl, gerundet auf zwei Nachkommastellen, zwischen den beiden input Werten.
    */
   private static double generateRoundedRandomInBounds(double lowerBound, double upperBound)
   {
      double generatedValue = lowerBound + (Math.random() * (upperBound - lowerBound));
      return roundToTwoDecimals(generatedValue);
   }

   /**
    * Rundet einen double-Wert auf <b>zwei Nachkommastellen</b>
    * @param value Wert der gerundet werden soll.
    * @return auf zwei Nachkommastellen gerundeten double-Wert
    */
   private static double roundToTwoDecimals(double value)
   {
      double d = Math.pow(10, 2);
      return Math.round(value * d) / d;
   }

}
