package gui.job;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import resource.PlantResource;

/**
 * Logging Klasse für {@link Job}-Instanzen
 */
public class JobLogger
{
   private final Logger logger;

   private JobLogger(Class<? extends Job> jobClazz)
   {
      logger = LogManager.getLogger(jobClazz);
   }

   public static JobLogger of(Class<? extends Job> jobClazz)
   {
      return new JobLogger(jobClazz);
   }

   public void jobStarted()
   {
      logger.info("Job started");
   }

   public void jobFinished()
   {
      logger.info("Job finished");
   }

   public void jobExecutedFor(PlantResource plantResource, int timesExecuted)
   {
      logger.info("Job for: "+plantResource.name()+" has been " + timesExecuted+" times executed");
   }
}
