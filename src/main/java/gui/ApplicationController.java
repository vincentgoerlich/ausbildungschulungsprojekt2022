package gui;

import dao.CsvDao;
import entity.AbstractPlantEntity;
import entity.CornPlantEntity;
import entity.WheatPlantEntity;
import gui.farm.FarmButtonController;
import gui.farm.FarmController;
import gui.plant.PlantModel;
import gui.plant.PlantModelPrototypes;
import gui.plantfield.PlantFieldController;
import gui.util.ConstraintsUtil;
import resource.PlantResource;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ApplicationController {


    private JPanel contentPane;
    private FarmController farmController;
    private FarmButtonController jobButtonCtrl;

    public ApplicationController() {
        initContentPane();
        initJobButtonCtrl();
        buildFarmController();
        wireJobButtonToFarm();
        layoutSubCtrls();
    }

    private void initJobButtonCtrl() {
        this.jobButtonCtrl = new FarmButtonController();
    }

    private void initContentPane() {
        contentPane = new JPanel(new GridBagLayout());
        contentPane.setBackground(Color.WHITE);
    }


    private void buildFarmController() {
        farmController = new FarmController();
        List<CornPlantEntity> cornPlantEntities = loadEntities(PlantResource.CORN);
        List<WheatPlantEntity> wheatPlantEntities = loadEntities(PlantResource.WHEAT);
        populatePlantFieldController(farmController.getCornFieldController(), cornPlantEntities);
        populatePlantFieldController(farmController.getWheatFieldController(), wheatPlantEntities);
    }

    private <PLANT_ENTITY extends AbstractPlantEntity> List<PLANT_ENTITY> loadEntities(PlantResource plantResource) {
        return new CsvDao().load(plantResource);
    }

    private <PLANT_ENTITY extends AbstractPlantEntity> void populatePlantFieldController(PlantFieldController<PLANT_ENTITY> plantFieldController, List<PLANT_ENTITY> plantEntities) {
        for (PLANT_ENTITY plantEntity : plantEntities) {
            PlantModel<PLANT_ENTITY> plantModel = PlantModelPrototypes.withEntity(plantEntity);
            plantFieldController.registerModel(plantModel);
        }
    }

    private void wireJobButtonToFarm() {
        jobButtonCtrl.registerPlantFieldCtrl(farmController::getWheatFieldController);
        jobButtonCtrl.registerPlantFieldCtrl(farmController::getCornFieldController);
    }


    private void layoutSubCtrls() {
        contentPane.add(farmController.getView(), ConstraintsUtil.constraints(0, 0));
        contentPane.add(jobButtonCtrl.getView(), ConstraintsUtil.constraints(1, 0));
    }

    public JPanel getView() {
        return this.contentPane;
    }

}
