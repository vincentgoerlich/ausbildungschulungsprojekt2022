package gui.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.awt.*;

/**
 * Convenience Util, die Methoden für {@link GridBagConstraints}-Objektgenerierung bereitstellt.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstraintsUtil
{

   public static GridBagConstraints constraintsWithWidth(int gridx, int gridy, int gridWidth, int gridHight){
      return constraints(gridx,gridy,
         gridWidth,
         gridHight,
         1.0,
         1.0,
         GridBagConstraints.CENTER,GridBagConstraints.BOTH,
         new Insets(0,0,0,0),
         0,0);
   }


   public static GridBagConstraints constraints(int gridx, int gridy){
      return constraints(gridx,gridy,
         1,
         1,
         1.0,
         1.0,
         GridBagConstraints.CENTER,GridBagConstraints.BOTH,
         new Insets(5,5,5,5),
         0,0);
   }

   public static GridBagConstraints constraints(int gridx, int gridy,
      int gridwidth, int gridheight,
      double weightx, double weighty,
      int anchor, int fill,
      Insets insets, int ipadx, int ipady){
      return new GridBagConstraints(gridx,gridy,gridwidth,gridheight,weightx,weighty,anchor,fill,insets,ipadx,ipady);
   }
}
