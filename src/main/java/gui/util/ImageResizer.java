package gui.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.swing.*;
import java.awt.*;

/**
 * Mini Util um ImageIcons neu zu skalieren.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ImageResizer
{

   public static ImageIcon to(ImageIcon original, int width, int height)
   {
      Image originalImage = original.getImage();
      Image resizedImage = originalImage.getScaledInstance(width, height, Image.SCALE_REPLICATE);
      return new ImageIcon(resizedImage);
   }
}

