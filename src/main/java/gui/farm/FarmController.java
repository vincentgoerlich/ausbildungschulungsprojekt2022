package gui.farm;

import entity.CornPlantEntity;
import entity.WheatPlantEntity;
import gui.plantfield.PlantFieldController;
import gui.util.ConstraintsUtil;
import lombok.Getter;
import resource.PlantResource;

import javax.swing.*;
import java.awt.*;
import java.util.function.Supplier;

/**
 * Der FarmController regelt die Darstellung von zwei {@link PlantFieldController}, indem er sie
 * nebeneinander layoutet und bildet somit die gesamt-View für alle {@link gui.plant.PlantController}
 */
@Getter
public class FarmController
{
   private final PlantFieldController<CornPlantEntity> cornFieldController;
   private final PlantFieldController<WheatPlantEntity> wheatFieldController;
   private final FarmView farmView;

   public FarmController()
   {
      farmView = new FarmView();
      wheatFieldController = new PlantFieldController<>(PlantResource.WHEAT);
      cornFieldController = new PlantFieldController<>(PlantResource.CORN);
      pushToView(wheatFieldController::getView);
      pushToView(cornFieldController::getView);
   }

   private void pushToView(Supplier<JPanel> viewSupplier)
   {
      farmView.addPlantFieldView(viewSupplier);
   }


   public JPanel getView(){
      return farmView.getView();
   }

   /**
    * View für {@link FarmController}. Layoutet maximal 2 PlantFieldViews.
    */
   private static class FarmView
   {
      private int viewsAdded = 0;
      private final JPanel contentPane;

      public FarmView()
      {
         contentPane = new JPanel(new GridBagLayout());
         contentPane.setBackground(Color.WHITE);
      }

      public void addPlantFieldView(Supplier<JPanel> plantFieldView)
      {
         if(viewsAdded < 2)
         {
            contentPane.add(plantFieldView.get(), ConstraintsUtil.constraints(viewsAdded, 0));
            viewsAdded++;
         }
      }

      public JPanel getView(){
         return contentPane;
      }
   }
}
