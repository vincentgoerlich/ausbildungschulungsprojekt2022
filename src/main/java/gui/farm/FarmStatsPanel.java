package gui.farm;

import gui.job.HarvestJob;
import gui.util.ConstraintsUtil;
import lombok.Getter;
import resource.PlantImageIconResource;

import javax.swing.*;
import java.awt.*;

/**
 * Stellt die Anzahl der geernteten Pflanzen der aktuellen Session dar.
 * Bedient sich dabei der statischen Felder des {@link HarvestJob}s. Wird durch {@link gui.job.FullCycleJob} getriggert.
 */
@Getter
public class FarmStatsPanel
{
   private static final Font PLANT_DISPLAY_FONT = new Font(Font.DIALOG, Font.BOLD, 30);
   private JPanel view;
   private JLabel cornStatsDisplay;
   private JLabel wheatStatsDisplay;
   private JLabel headerDisplay;

   public FarmStatsPanel()
   {
      buildView();
      buildHeaderDisplay();
      buildWheatStatsDisplay();
      buildCornStatsDisplay();
      layoutDisplaysOnView();
      update();
   }

   private void layoutDisplaysOnView()
   {
      this.view.add(headerDisplay, ConstraintsUtil.constraintsWithWidth(0, 0, 2, 1));
      this.view.add(wheatStatsDisplay, ConstraintsUtil.constraints(0, 1));
      this.view.add(cornStatsDisplay, ConstraintsUtil.constraints(1, 1));
   }

   private void buildView()
   {
      this.view = new JPanel(new GridBagLayout());
      this.view.setBackground(Color.WHITE);
   }

   private void buildCornStatsDisplay()
   {
      this.cornStatsDisplay = new JLabel("", SwingConstants.CENTER);
      this.cornStatsDisplay.setIcon(PlantImageIconResource.CORN.getFinalStadiumIcon());
      this.cornStatsDisplay.setVerticalTextPosition(SwingConstants.BOTTOM);
      this.cornStatsDisplay.setHorizontalTextPosition(SwingConstants.CENTER);
      this.cornStatsDisplay.setFont(PLANT_DISPLAY_FONT);
   }

   private void buildWheatStatsDisplay()
   {
      this.wheatStatsDisplay = new JLabel("", SwingConstants.CENTER);
      this.wheatStatsDisplay.setIcon(PlantImageIconResource.WHEAT.getFinalStadiumIcon());
      this.wheatStatsDisplay.setVerticalTextPosition(SwingConstants.BOTTOM);
      this.wheatStatsDisplay.setHorizontalTextPosition(SwingConstants.CENTER);
      this.wheatStatsDisplay.setFont(PLANT_DISPLAY_FONT);
   }

   private void buildHeaderDisplay()
   {
      this.headerDisplay = new JLabel("Harvest Stats For this Session", SwingConstants.CENTER);
      this.headerDisplay.setFont(PLANT_DISPLAY_FONT);
   }

   /**
    * Setzt die aktuellen Erntewerte des {@link HarvestJob}s auf die entsprechenden Displays.
    */
   public void update()
   {
      String cornDisplayString = String.valueOf(HarvestJob.getCornHarvested());
      this.cornStatsDisplay.setText(cornDisplayString);
      String wheatDisplayString = String.valueOf(HarvestJob.getWheatHarvested());
      this.wheatStatsDisplay.setText(wheatDisplayString);
   }
}
