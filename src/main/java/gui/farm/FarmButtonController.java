package gui.farm;

import gui.builder.ButtonPanel;
import gui.builder.EasyButtonPanelBuilder;
import gui.customcomponent.RoundedRectangleButton;
import gui.job.FullCycleJob;
import gui.plantfield.PlantFieldController;
import gui.util.ConstraintsUtil;
import resource.MenuImageIconResource;

import javax.swing.*;
import java.awt.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Controller-Klasse & View der rechten Bildschirmhälfte. Zuständig für die Darstellung und "Verdrahtung" der Buttons und Dart der Stats,
 */
public class FarmButtonController
{

   private final JPanel view;
   private final FarmStatsPanel statsPanelController;
   private final List<Supplier<PlantFieldController<?>>> plantFieldCtrlSuppliers;
   private final FullCycleJob fullCycleJob;

   public FarmButtonController()
   {
      this.view = new JPanel(new GridBagLayout());
      this.plantFieldCtrlSuppliers = new ArrayList<>();
      this.statsPanelController = new FarmStatsPanel();
      this.fullCycleJob = new FullCycleJob(() -> statsPanelController);
      initView();
   }

   private void initView()
   {
      this.view.add(buildFullCycleButton().getView(), ConstraintsUtil.constraintsWithWidth(0, 0, 2, 2));
      this.view.add(buildExportButtonPanel().getView(), ConstraintsUtil.constraintsWithWidth(0, 2, 2, 2));
      this.view.add(this.statsPanelController.getView(), ConstraintsUtil.constraintsWithWidth(0, 4, 2, 2));
   }

   private ButtonPanel buildFullCycleButton()
   {
      return EasyButtonPanelBuilder
         .horizontalLayout()
         .addButton(new RoundedRectangleButton())
         .withText("Full Cycle")
         .withIcon(MenuImageIconResource.FULL_CYCLE_JOB.getIcon())
         .withToolTip("Performs Full Cycle on Fields")
         .withActionListener(e -> executeAllJobs())
         .finishBuild();
   }

   private ButtonPanel buildExportButtonPanel()
   {
      ButtonPanel buttonPanel = EasyButtonPanelBuilder
         .horizontalLayout()
         .addButton(new RoundedRectangleButton())
         .withText("Export to .json")
         .withIcon(MenuImageIconResource.EXPORT_JSON.getIcon())
         .withToolTip("Exports data to .json files")
         .withActionListener(e -> trollFrede())
         .finishBuild();

      buttonPanel.addListenerToButton(0, e ->
      {
         buttonPanel.getButton(0).setIcon(MenuImageIconResource.RACOON.getIcon());
         buttonPanel.getButton(0).setText("Rick Rolled");
      });
      return buttonPanel;
   }

   private void trollFrede()
   {

      Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
      if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
      {
         try
         {
            desktop.browse(URI.create("https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1"));
         }
         catch(Exception e)
         {
            e.printStackTrace();
         }

      }

   }

   /**
    * Fügt ein Pflanzenfeld hinzu, welches mit dem Fullcycle-button implizit "verdrahtet" wird.
    * @param plantFieldCtrl pflanzenfeld, auf welches der Fullcycle button Zugriff haben soll
    */
   public void registerPlantFieldCtrl(Supplier<PlantFieldController<?>> plantFieldCtrl)
   {
      this.plantFieldCtrlSuppliers.add(plantFieldCtrl);
   }

   /**
    * Parallele Bearbeitung beider Felder
    */
   private void executeAllJobs()
   {
      for(Supplier<PlantFieldController<?>> plantFieldCtrlSupplier : plantFieldCtrlSuppliers)
      {
         new Thread(() -> fullCycleJob.execute(plantFieldCtrlSupplier.get())).start();
      }
   }

   public JPanel getView()
   {
      return view;
   }

}
