package gui.builder;

import gui.util.ConstraintsUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * ButtonPanel, welches hinzugefügte {@link JButton}-Instanzen automatisch entweder <b>vertikal</b> oder <b>horizontal</b> layoutet.
 */
public class ButtonPanel {

    private final JPanel view;
    private final List<JButton> buttons;
    private final boolean isVerticalLayout;

    public ButtonPanel(boolean isVerticalLayout) {
        this.view = new JPanel(new GridBagLayout());
        this.view.setBackground(Color.WHITE);
        this.buttons = new ArrayList<>();
        this.isVerticalLayout = isVerticalLayout;
    }

    /**
     * Fügt button hinzu und layoutet alle bereits hinzugefügten buttons neu.
     * @param jButton neuer button fürs Panel
     */
    public void addButton(JButton jButton) {
        this.buttons.add(jButton);
        layoutButtons();
    }

    /**
     * Erlaubt das nachträgliche hinzufügen von einem ActionListener für einen button.
     * @param index index des bereits hinzugefügten Buttons.
     * @param actionListener actionlistener für den bereits hinzufügten button.
     */
    public void addListenerToButton(int index, ActionListener actionListener) {
        buttons.get(index).addActionListener(actionListener);
    }


    public JButton getButton(int index){
        return this.buttons.get(index);
    }

    public JPanel getView() {
        return view;
    }

    private void layoutButtons() {
        for (int i = 0; i < buttons.size(); i++) {
            int x = isVerticalLayout ? 0 : i;
            int y = isVerticalLayout ? i : 0;
            GridBagConstraints constraints = ConstraintsUtil.constraints(x, y);
            this.view.add(buttons.get(i), constraints);
        }

    }
}
