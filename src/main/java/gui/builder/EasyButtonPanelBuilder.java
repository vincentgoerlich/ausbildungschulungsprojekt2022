package gui.builder;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * <b>Kleiner API - Versuch</b> Die ungenutzen Methoden bleiben :D  <br>
 * Fluent Builder, der in Kooperation mit {@link EasyButtonPanelBuilder.EasyButtonBuilder} dem Anwender erlaubt
 * ein {@link JPanel} mit beliebig vielen {@link JButton}-Instanzen zu versehen und diese automatisch mittels {@link GridBagLayout} & {@link GridBagConstraints}
 * zu layouten.
 * <pre>
 *
 * Bsp.:
 *
 * <code>JPanel buttonPanel =
 * EasyButtonPanelBuilder.horizontalLayout()
 *                                    .addButton(new RoundedRectangleButton())
 *                                    .withText("Export to .csv")
 *                                    .withToolTip("Exports data to .csv files")
 *                                    .withActionListener(e -> exportToCsv())
 *                                    .and()
 *                                    .addButton(new RoundedRectangleButton())
 *                                    .withText("Export to .json")
 *                                    .withToolTip("Exports data to .json files")
 *                                    .withActionListener(e -> exportToJson())
 *                                    .finishBuild();</code>
 *                            </pre>
 */
public class EasyButtonPanelBuilder
{
    private final ButtonPanel buttonPanel;
    private final EasyButtonBuilder buttonBuilder;

    /**
     * @return ein Objekt dieser Klasse, welches jeden angebrachten Button <b>vertikal</b> anordnet
     */
    public static EasyButtonPanelBuilder verticalLayout()
    {
        return new EasyButtonPanelBuilder(true);
    }

    /**
     * @return ein Objekt dieser Klasse, welches jeden angebrachten Button <b>horizontal</b> anordnet
     */
    public static EasyButtonPanelBuilder horizontalLayout()
    {
        return new EasyButtonPanelBuilder(false);
    }

    private EasyButtonPanelBuilder(boolean vertical)
    {
        buttonPanel = new ButtonPanel(vertical);
        buttonBuilder = new EasyButtonBuilder(this);
    }

    /**
     * Erlaubt dem Anwender jegliche {@link JButton}-Instanz als nächsten Button zu dekorieren und aufs {@link EasyButtonPanelBuilder#buttonPanel} zu bringen
     *
     * @param jButton gewünschte {@link JButton}-Instanz
     * @return {@link EasyButtonPanelBuilder.EasyButtonBuilder} mit dem der Input Button dekoriert werden kann
     */
    public EasyButtonBuilder addButton(JButton jButton)
    {
        this.buttonBuilder.buttonBuild = jButton;
        return this.buttonBuilder;
    }
    /**
     * Je nach {@link EasyButtonPanelBuilder#verticalLayout()}/{@link EasyButtonPanelBuilder#horizontalLayout()}-Instanziierung,
     * layoutet die input {@link JButton}-Instanz automatisch
     *
     * @param  jButton {@link JButton}-Instanz, der dem {@link EasyButtonPanelBuilder#buttonPanel} hinzugefügt und gelayoutet werden soll
     */
    private void layoutButton(JButton jButton){
        buttonPanel.addButton(jButton);
    }



    /**
     * Finale Methode, die den Building Prozess terminiert und das dekorierte Panel zurückgibt
     *
     * @return fertig dekoriertes {@link JPanel}
     * @see EasyButtonBuilder#finishBuild()
     */
    public ButtonPanel finishBuild()
    {
        return this.buttonPanel;
    }

    /**
     * Innerer Fluent-Builder, welcher durch den {@link EasyButtonPanelBuilder} eine {@link JButton}-Instanz entgegen nimmt
     * und diesen so lange weiter verarbeitet, bis {@link EasyButtonBuilder#and()} für weiteren button oder {@link EasyButtonBuilder#finishBuild()}}
     * zum Zurückgeben des fertigen {@link EasyButtonPanelBuilder#buttonPanel} gecalled wird
     */
    public static class EasyButtonBuilder
    {
        private final EasyButtonPanelBuilder easyButtonPanelBuilder;
        private JButton buttonBuild;

        EasyButtonBuilder(EasyButtonPanelBuilder easyButtonPanelBuilder)
        {
            this.easyButtonPanelBuilder = easyButtonPanelBuilder;
        }

        /**
         * Stattet den {@link EasyButtonBuilder#buttonBuild} mit einer Beschriftung aus
         *
         * @param text Beschriftung des {@link EasyButtonBuilder#buttonBuild}
         * @return {@link EasyButtonBuilder} zur Weiterverarbeitung des {@link EasyButtonBuilder#buttonBuild}
         */
        public EasyButtonBuilder withText(String text)
        {
            buttonBuild.setText(text);
            return this;
        }

        /**
         * Stattet den {@link EasyButtonBuilder#buttonBuild} mit einem {@link ImageIcon} aus
         *
         * @param imageIcon {@link ImageIcon} für {@link EasyButtonBuilder#buttonBuild}
         * @return {@link EasyButtonPanelBuilder.EasyButtonBuilder} zur Weiterverarbeitung des {@link EasyButtonBuilder#buttonBuild}
         */
        public EasyButtonBuilder withIcon(ImageIcon imageIcon)
        {
            buttonBuild.setIcon(imageIcon);
            return this;
        }
        /**
         * Stattet den {@link EasyButtonBuilder#buttonBuild} mit einem {@link ActionListener} aus
         *
         * @param actionListener {@link ActionListener} für {@link EasyButtonBuilder#buttonBuild}
         * @return {@link EasyButtonPanelBuilder.EasyButtonBuilder} zur Weiterverarbeitung des {@link EasyButtonPanelBuilder.EasyButtonBuilder#buttonBuild}
         */

        public EasyButtonBuilder withActionListener(ActionListener actionListener)
        {
            buttonBuild.addActionListener(actionListener);
            return this;
        }

        /**
         * Stattet den {@link EasyButtonBuilder#buttonBuild} mit einem Tooltip aus
         *
         * @param toolTip Tooltip für {@link EasyButtonBuilder#buttonBuild}
         * @return {@link EasyButtonBuilder} zur Weiterverarbeitung des {@link EasyButtonBuilder#buttonBuild}
         */
        public EasyButtonBuilder withToolTip(String toolTip)
        {
            buttonBuild.setToolTipText(toolTip);
            return this;
        }

        /**
         * Layoutet den Button auf dem {@link EasyButtonPanelBuilder#buttonPanel} und lässt den Anwender weitere Aufrufe vom {@link EasyButtonPanelBuilder} aus tätigen
         *
         * @return {@link EasyButtonPanelBuilder} zur Weiterverarbeitung.
         */
        public EasyButtonPanelBuilder and()
        {
            easyButtonPanelBuilder.layoutButton(buttonBuild);
            return easyButtonPanelBuilder;
        }
        /**
         * Finaler Aufruf, beendet den build Prozess und liefert das fertige {@link EasyButtonPanelBuilder#buttonPanel} zurück
         *
         * @return {@link EasyButtonPanelBuilder#buttonPanel}
         */
        public ButtonPanel finishBuild()
        {
            return and().finishBuild();
        }
    }


}
