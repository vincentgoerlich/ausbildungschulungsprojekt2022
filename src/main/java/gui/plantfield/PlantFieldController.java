package gui.plantfield;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import entity.AbstractPlantEntity;
import gui.plant.PlantController;
import gui.plant.PlantModel;
import gui.util.ConstraintsUtil;
import lombok.Getter;
import lombok.Setter;
import resource.PlantResource;

/**
 * Übergeordneter Controller mit 50 vorgefertigten {@link PlantController}. Layoutet diese und organisiert die Darstellung.
 * @param <PLANT_ENTITY>  {@link AbstractPlantEntity}-Instanz
 */
@Getter
@Setter
public class PlantFieldController<PLANT_ENTITY extends AbstractPlantEntity>
{
   private PlantFieldView plantView;
   private List<PlantController<PLANT_ENTITY>> plantControllers;
   private final PlantResource plantResource;

   public PlantFieldController(PlantResource plantResource)
   {
      this.plantResource = plantResource;
      this.plantView = new PlantFieldView(plantResource);
      this.plantControllers = new ArrayList<>();
      initAndLayoutFiftyPlantController();
   }

   /**
    * Sollte wenigstens ein {@link PlantController} ein null {@link PlantModel} haben,
    * wird dieses Model durch den input ersetzt. Ansonsten passiert nichts.
    * @param plantModel neues {@link PlantModel}
    */
   public void registerModel(PlantModel<PLANT_ENTITY> plantModel)
   {
      PlantController<PLANT_ENTITY> withOutModel = findControllerWithOutModelOrNull();
      if(findControllerWithOutModelOrNull() != null)
      {
         withOutModel.setPlantModel(plantModel);
      }
   }

   private PlantController<PLANT_ENTITY> findControllerWithOutModelOrNull()
   {
      return plantControllers.stream().filter(PlantController::plantModelIsNull).findFirst().orElse(null);
   }

   private void initAndLayoutFiftyPlantController()
   {
      for(int i = 0; i < 50; i++)
      {
         plantControllers.add(new PlantController<>());
      }
      pushToView();
   }

   private void pushToView()
   {
      for(PlantController<PLANT_ENTITY> plantController : plantControllers)
      {
         pushToView(plantController);
      }
   }

   private void pushToView(PlantController<PLANT_ENTITY> plantController)
   {
      this.plantView.addAndAutoLayout(plantController.getView());
   }

   public JPanel getView()
   {
      return this.plantView.getView();
   }

   private static class PlantFieldView
   {
      private static final int MAX_COLUMN = 5;
      private static final int MAX_ROW = 10;
      private int columnCache = 0;
      private int rowCache = 1;

      private final JPanel contentPane;

      public PlantFieldView(PlantResource plantResource)
      {
         contentPane = new JPanel(new GridBagLayout());
         //contentPane.setBorder(new LineBorder(Color.BLACK));
         contentPane.setBackground(Color.WHITE);
         initAndLayoutHeader(plantResource);
      }

      private void initAndLayoutHeader(PlantResource plantResource)
      {
         String printableFormat = plantResource.name().toLowerCase(Locale.ROOT);
         JLabel headerLbl = new JLabel(printableFormat + " field", SwingConstants.CENTER);
         headerLbl.setFont(new Font(Font.DIALOG, Font.BOLD, 30));

         headerLbl.setHorizontalTextPosition(SwingConstants.CENTER);
         contentPane.add(headerLbl, ConstraintsUtil.constraintsWithWidth(0, 0, 5, 1));
      }

      public void addAndAutoLayout(JLabel plantView)
      {
         if(gridIsNotFull())
         {
            contentPane.add(plantView, ConstraintsUtil.constraints(columnCache, rowCache));

            if(endOfRowHit())
            {
               nextColumn();
               resetRowCache();
            }
            else
            {
               nextRow();
            }
         }
      }

      private void nextRow()
      {
         rowCache++;
      }

      private boolean gridIsNotFull()
      {
         return columnCache != MAX_COLUMN;
      }

      private boolean endOfRowHit()
      {
         return rowCache == MAX_ROW;
      }

      private void resetRowCache()
      {
         rowCache = 1;
      }

      private void nextColumn()
      {
         columnCache++;
      }

      public JPanel getView()
      {
         return contentPane;
      }
   }

}
