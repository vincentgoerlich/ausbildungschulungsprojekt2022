package testdata;

import entity.AbstractPlantEntity;
import entity.PlantEntityPrototypes;
import gui.plant.PlantController;
import gui.plant.PlantModel;
import gui.plantfield.PlantFieldController;
import resource.PlantResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory um Testobjekte zu generieren.
 */
public class TestDataFactory
{

   public static <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY createPlantEntity(PlantResource plantResource)
   {
      return PlantEntityPrototypes.of(plantResource);
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY createPlantEntity(PlantResource plantResource, double height)
   {
      PLANT_ENTITY plantEntity = createPlantEntity(plantResource);
      plantEntity.setCurrentHeight(height);
      return plantEntity;
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantModel<PLANT_ENTITY> createPlantModel(PlantResource plantResource)
   {
      return new PlantModel<>(createPlantEntityWithRandomHeight(plantResource));
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantController<PLANT_ENTITY> createPlantControllerWithNonNullModelAndEntity(PlantResource plantResource)
   {
      return new PlantController<>(createPlantModel(plantResource));
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantController<PLANT_ENTITY> createPlantControllerWithNullModel(PlantResource plantResource)
   {
      return new PlantController<>();
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantFieldController<PLANT_ENTITY> createPlantFieldCtrl(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = new PlantFieldController<>(plantResource);
      for(int i = 0; i < 50; i++)
      {
         plantFieldController.registerModel(createPlantModel(plantResource));
      }
      return plantFieldController;
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantFieldController<PLANT_ENTITY> createPlantFieldCtrlWith25NullAnd25NonNullModels(
      PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = new PlantFieldController<>(plantResource);
      for(int i = 0; i < 50; i++)
      {
         PlantModel<PLANT_ENTITY> plantModel = i < 25 ? createPlantModel(plantResource) : null;
         plantFieldController.registerModel(plantModel);
      }
      return plantFieldController;
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PLANT_ENTITY createPlantEntityWithRandomHeight(PlantResource plantResource)
   {
      return createPlantEntity(plantResource, (Math.random() * 200));
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantFieldController<PLANT_ENTITY> createPlantFieldCtrl25Harvestable(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = new PlantFieldController<>(plantResource);
      for(int i = 0; i < 50; i++)
      {
         PlantModel<PLANT_ENTITY> plantModel = createPlantModel(plantResource);
         double height = i < 25 ? 101 : 99;
         plantModel.getPlantEntity().setCurrentHeight(height);
         plantFieldController.registerModel(plantModel);
      }
      return plantFieldController;
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> PlantFieldController<PLANT_ENTITY> createPlantFieldCtrl25Sowable(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = new PlantFieldController<>(plantResource);
      for(int i = 0; i < 50; i++)
      {
         PlantModel<PLANT_ENTITY> plantModel = i < 25 ? createPlantModel(plantResource) : null;
         plantFieldController.registerModel(plantModel);
      }
      return plantFieldController;
   }

   public static <PLANT_ENTITY extends AbstractPlantEntity> List<PLANT_ENTITY> createPlantEntitiesWithRandomHeight(PlantResource plantResource)
   {
      List<PLANT_ENTITY> plantEntities = new ArrayList<>(50);
      for(int i = 0; i < 50; i++)
      {
         plantEntities.add(createPlantEntityWithRandomHeight(plantResource));
      }
      return plantEntities;
   }
}
