package gui.plantfield;

import entity.AbstractPlantEntity;
import gui.plant.PlantController;
import gui.plant.PlantModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import resource.PlantResource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static testdata.TestDataFactory.*;

public class PlantFieldControllerTest
{
   private static final int FULL_FIELD_SIZE = 50;

   @ParameterizedTest
   @EnumSource(PlantResource.class)
   @DisplayName("new PlantFieldController() has 50 PlantControllers initialized")
   <PLANT_ENTITY extends AbstractPlantEntity> void constructorTest(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = new PlantFieldController<>(plantResource);
      assertEquals(FULL_FIELD_SIZE, plantFieldController.getPlantControllers().size());
   }

   @ParameterizedTest
   @EnumSource(PlantResource.class)
   @DisplayName("registerModel() with valid input and space available allows for registering")
   <PLANT_ENTITY extends AbstractPlantEntity> void registerModel1(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = createPlantFieldCtrlWith25NullAnd25NonNullModels(plantResource);
      int nullModelsBefore = countNullModels(plantFieldController);

      plantFieldController.registerModel(createPlantModel(plantResource));

      int nullModelsAfter = countNullModels(plantFieldController);

      assertEquals(nullModelsAfter, nullModelsBefore - 1);
   }

   @ParameterizedTest
   @EnumSource(PlantResource.class)
   @DisplayName("registerModel() allows for registering, if it holds at least one PlantController with a null PlantModel")
   <PLANT_ENTITY extends AbstractPlantEntity> void registerModel2(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = createPlantFieldCtrlWith25NullAnd25NonNullModels(plantResource);
      int beforeRegistering = countNullModels(plantFieldController);
      PlantModel<PLANT_ENTITY> plantModel = createPlantModel(plantResource);
      plantFieldController.registerModel(plantModel);
      int afterRegistering = countNullModels(plantFieldController);
      assertEquals(afterRegistering, beforeRegistering - 1);
   }

   @ParameterizedTest
   @EnumSource(PlantResource.class)
   @DisplayName("registerModel() will not register a new Model, if it holds only PlantControllers with non null Plantmodels")
   <PLANT_ENTITY extends AbstractPlantEntity> void registerModel3(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = createPlantFieldCtrl(plantResource);
      int beforeRegistering = countNullModels(plantFieldController);
      PlantModel<PLANT_ENTITY> plantModel = createPlantModel(plantResource);
      plantFieldController.registerModel(plantModel);
      int afterRegistering = countNullModels(plantFieldController);
      assertEquals(afterRegistering, beforeRegistering);
   }

   public <PLANT_ENTITY extends AbstractPlantEntity> int countNullModels(PlantFieldController<PLANT_ENTITY> plantFieldController)
   {
      return (int) plantFieldController.getPlantControllers().stream().filter(PlantController::plantModelIsNull).count();
   }

}
