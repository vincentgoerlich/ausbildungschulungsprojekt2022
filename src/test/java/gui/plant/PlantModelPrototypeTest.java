package gui.plant;

import entity.AbstractPlantEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mockito;
import resource.PlantResource;

import static org.junit.jupiter.api.Assertions.*;

public class PlantModelPrototypeTest
{

   @Test
   @DisplayName("of() throws IllegalArgumentException if input not mapped")
   void ofThrowsIllegalArgumentExceptionIfInputNotMapped()
   {
      PlantResource plantResource = Mockito.mock(PlantResource.class);
      assertThrows(IllegalArgumentException.class, () -> PlantModelPrototypes.of(plantResource));
   }

   @ParameterizedTest(name = "#{index} | {0}")
   @EnumSource(PlantResource.class)
   @DisplayName("of() produces with valid input corresponding PlantModel & PlantEntity with all default values")
   <PLANT_ENTITY extends AbstractPlantEntity> void ofProducesCorrectPlantModel(PlantResource plantResource)
   {
      PlantModel<PLANT_ENTITY> plantModel = PlantModelPrototypes.of(plantResource);
      assertNotNull(plantModel);
      assertEquals(0.0, plantModel.getCurrentHeight());
      assertEquals(plantResource, plantModel.getPlantResource());
   }
}
