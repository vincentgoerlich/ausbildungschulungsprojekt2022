package dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import entity.AbstractPlantEntity;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.junit.jupiter.MockitoExtension;
import resource.PlantResource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.Mockito.*;
import static testdata.TestDataFactory.createPlantEntitiesWithRandomHeight;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CsvDaoTest
{
   private static CsvDao csvDao;
   private static final File CORN_SOURCE_FILE = new File("src/test/java/testdata/cornTestData.csv");
   private static final File WHEAT_SOURCE_FILE = new File("src/test/java/testdata/wheatTestData.csv");

   @BeforeAll
   static void setUp()
   {
      csvDao = mock(CsvDao.class);
      // Test csv Dateien statt original csv-Dateien nehmen
      when(csvDao.autoChooseFile(PlantResource.CORN)).thenReturn(CORN_SOURCE_FILE);
      when(csvDao.autoChooseFile(PlantResource.WHEAT)).thenReturn(WHEAT_SOURCE_FILE);
      // Gesamtes Lade- und Speicherverhalten soll nicht gemockt, sondern original ausgeführt werden.
      doCallRealMethod().when(csvDao).load(any());
      doCallRealMethod().when(csvDao).persistEntities(any());
      doCallRealMethod().when(csvDao).save(any());
      doCallRealMethod().when(csvDao).tryToLoadAndConvertOrEmptyList(any());
   }

   @ParameterizedTest(name = "#{index} {0} successfully saved")
   @EnumSource(PlantResource.class)
   @DisplayName("save() creates new File, if it does not exist and if load()-tests works, with correct values as well")
   @Order(1)
   <PLANT_ENTITY extends AbstractPlantEntity> void savingWorks(PlantResource plantResource)
   {
      // es darf noch keine Datei exisitieren.
      assertFalse(csvDao.autoChooseFile(plantResource).exists());
      List<PLANT_ENTITY> plantEntities = createPlantEntitiesWithRandomHeight(plantResource);
      // Datei wird erstellt und geschrieben
      csvDao.save(plantEntities);
      assertTrue(csvDao.autoChooseFile(plantResource).exists());
   }

   @ParameterizedTest(name = "#{index} {0} has fifty plantEntities")
   @EnumSource(PlantResource.class)
   @DisplayName("load() if file exists, fifty AbstractPlantEntities will be loaded")
   @Order(2)
   void loadingProvidesEntitiesIfFileExistsAndHasValues(PlantResource plantResource)
   {
      // Beide Dateien müssen existieren und 50 Inhalt haben
      assumeTrue(CORN_SOURCE_FILE.exists() && WHEAT_SOURCE_FILE.exists());
      assertEquals(50, csvDao.load(plantResource).size());
   }

   @ParameterizedTest(name = "#{index} {0} plant entities have correct values")
   @EnumSource(PlantResource.class)
   @DisplayName("load() if file exists, loading provides AbstractPlantEntity instances with correct values")
   @Order(3)
   <PLANT_ENTITY extends AbstractPlantEntity> void loadedEntitiesHaveValues(PlantResource plantResource)
   {
      assumeTrue(CORN_SOURCE_FILE.exists() && WHEAT_SOURCE_FILE.exists());
      List<Executable> allAssertions = new ArrayList<>();

      List<PLANT_ENTITY> plantEntities = csvDao.load(plantResource);

      for(PLANT_ENTITY plantEntity : plantEntities)
      {
         allAssertions.add(() -> assumeTrue(plantEntity.getCurrentHeight() != 0.0));
         allAssertions.add(() -> assertNotNull(plantEntity.getPlantResource()));
      }
      // Hat den Charme, dass alle assertions, selbst bei fail ausgewertet werden.
      assertAll(allAssertions);
   }

   @AfterAll
   static void takeDown()
   {
      CORN_SOURCE_FILE.delete();
      WHEAT_SOURCE_FILE.delete();
   }

}
