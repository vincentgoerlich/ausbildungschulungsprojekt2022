package entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.Mockito;
import resource.PlantResource;

import static org.junit.jupiter.api.Assertions.*;

public class PlantEntityPrototypeTest
{

   @ParameterizedTest(name = "#{index} | {0}")
   @EnumSource(PlantResource.class)
   @DisplayName("of() does not throw ClassCastException")
   void ofDoesNotThrowClassCastException(PlantResource plantResource)
   {
      assertDoesNotThrow(() -> PlantEntityPrototypes.of(plantResource));
   }

   @Test
   @DisplayName("of() throws IllegalArgumentException if input not mapped")
   void ofThrowsIllegalArgumentExceptionIfInputNotMapped()
   {
      PlantResource plantResource = Mockito.mock(PlantResource.class);
      assertThrows(IllegalArgumentException.class, () -> PlantEntityPrototypes.of(plantResource));
   }

   @ParameterizedTest(name = "#{index} | {0}")
   @EnumSource(PlantResource.class)
   @DisplayName("of() produces with valid input corresponding AbstractPlantEntity-instance with all default values")
   <PLANT_ENTITY extends AbstractPlantEntity> void ofProducesWithCornInputCornentityWithAllDefaultValues(PlantResource plantResource)
   {
      PLANT_ENTITY plant_entity = PlantEntityPrototypes.of(plantResource);
      assertNotNull(plant_entity);
      assertEquals(0.0, plant_entity.getCurrentHeight());
      assertEquals(plantResource, plant_entity.getPlantResource());
   }
}
