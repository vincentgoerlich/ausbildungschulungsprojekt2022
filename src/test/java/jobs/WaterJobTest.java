package jobs;

import entity.AbstractPlantEntity;
import gui.job.WaterJob;
import gui.plant.PlantController;
import gui.plant.PlantModel;
import gui.plantfield.PlantFieldController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import resource.PlantResource;
import testdata.TestDataFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WaterJobTest
{
   private static WaterJob waterJob;

   @BeforeAll
   static void setUp()
   {
      waterJob = new WaterJob();
   }

   @ParameterizedTest(name = "#{index} {0} growth within limits")
   @EnumSource(PlantResource.class)
   @DisplayName("waterjob lets all plants grow according to their growth limitations")
   <PLANT_ENTITY extends AbstractPlantEntity> void generic(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = TestDataFactory.createPlantFieldCtrl(plantResource);
      List<Double> heightsBefore = mapToHeightList(plantFieldController);
      waterJob.execute(plantFieldController);
      List<Double> heightAfter = mapToHeightList(plantFieldController);
      double minimumGrowth = plantResource.getMinimalGrowthInCmPerCycle();
      double maximumGrowth = plantResource.getMaximumGrowthInCmPerCycle();
      List<Executable> assertionsForMultiAssertion = new ArrayList<>();
      // kleiner Kunstgriff um alle assertions zu bauen und durchlaufen zu lassen
      for(int i = 0; i < plantFieldController.getPlantControllers().size(); i++)
      {
         double actualGrowth = heightAfter.get(i) - heightsBefore.get(i);
         assertionsForMultiAssertion.add(() -> assertTrue(isWithInBounds(actualGrowth, minimumGrowth, maximumGrowth)));
      }
      assertAll(assertionsForMultiAssertion);
   }

   private boolean isWithInBounds(double actual, double lowerBound, double upperBound)
   {
      return actual >= lowerBound && actual <= upperBound;
   }

   private static List<Double> mapToHeightList(PlantFieldController<?> plantFieldController)
   {
      return plantFieldController.getPlantControllers().stream()
         .map(PlantController::getPlantModel)
         .map(PlantModel::getCurrentHeight)
         .collect(Collectors.toList());
   }

}
