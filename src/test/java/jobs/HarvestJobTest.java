package jobs;

import entity.AbstractPlantEntity;
import gui.job.HarvestJob;
import gui.plant.PlantController;
import gui.plantfield.PlantFieldController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import resource.PlantResource;
import testdata.TestDataFactory;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HarvestJobTest
{
   private static HarvestJob harvestJob;

   @BeforeAll
   static void setUp()
   {
      harvestJob = new HarvestJob();
   }

   @ParameterizedTest(name = "#{index} {0} successfully harvested")
   @EnumSource(PlantResource.class)
   @DisplayName("harvestjob takes 25 harvestable fields and harvests them, and only them.")
   <PLANT_ENTITY extends AbstractPlantEntity> void testHarvestJob(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = TestDataFactory
         .createPlantFieldCtrl25Harvestable(plantResource);
      int beforeHarvest = countModelsNotNull(plantFieldController);
      harvestJob.execute(plantFieldController);
      int afterHarvest = countModelsNotNull(plantFieldController);
      int plantsLeftOnField = beforeHarvest- afterHarvest;
      assertEquals(25, plantsLeftOnField);
   }

   private int countModelsNotNull(PlantFieldController<?> plantFieldController)
   {
      return (int) plantFieldController.getPlantControllers().stream()
         .map(PlantController::getPlantModel)
         .filter(Objects::nonNull)
         .count();
   }

}
