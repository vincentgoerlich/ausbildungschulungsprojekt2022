package jobs;

import entity.AbstractPlantEntity;
import gui.job.SowJob;
import gui.plant.PlantController;
import gui.plantfield.PlantFieldController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import resource.PlantResource;
import testdata.TestDataFactory;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SowJobTest
{
   private static SowJob sowJob;

   @BeforeAll
   static void setUp()
   {
      sowJob = new SowJob();
   }

   @ParameterizedTest
   @EnumSource(PlantResource.class)
   @DisplayName("Field with 25 empty fields has zero empty fields after sowing.")
   <PLANT_ENTITY extends AbstractPlantEntity> void testingStuff(PlantResource plantResource)
   {
      PlantFieldController<PLANT_ENTITY> plantFieldController = TestDataFactory.createPlantFieldCtrl25Sowable(plantResource);
      int emptyFields = countNullModels(plantFieldController);
      sowJob.execute(plantFieldController);
      int emptyFieldsAfterJob = countNullModels(plantFieldController);
      assertAll(
         () -> assertEquals(25, emptyFields),
         () -> assertEquals(0, emptyFieldsAfterJob)
               );

   }

   private int countNullModels(PlantFieldController<?> plantFieldController)
   {
      return (int) plantFieldController.getPlantControllers().stream()
         .map(PlantController::getPlantModel)
         .filter(Objects::isNull)
         .count();
   }
}
